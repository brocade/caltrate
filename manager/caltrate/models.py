# -*- coding: utf-8 -*-

from django.db import models

# Create your models here.


class GiftCategory(models.Model):
    class Meta:
        db_table = "gift_category"

    def __unicode__(self):
        return self.name

    name = models.CharField(max_length=45)
    tablet = models.IntegerField()
    count = models.IntegerField()
    max = models.IntegerField()


class User(models.Model):
    class Meta:
        db_table = "user"

    def __unicode__(self):
        return self.nickname

    openid = models.CharField(max_length=100)
    nickname = models.CharField(max_length=50)
    sex = models.IntegerField()
    province = models.CharField(max_length=45)
    city = models.CharField(max_length=45)
    country = models.CharField(max_length=45)
    headimgurl = models.CharField(max_length=256)


class CrazyTablet(models.Model):
    class Meta:
        db_table = "crazytablet"


    def __unicode__(self):
        return self.name

    user = models.ForeignKey(User)
    gift_category = models.ForeignKey(GiftCategory)
    name = models.CharField(max_length=45)
    cell = models.CharField(max_length=45)
    address = models.CharField(max_length=256)
