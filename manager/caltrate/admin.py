# -*- coding: utf-8 -*-


from django.contrib import admin
from django.core.urlresolvers import reverse

from import_export import resources
from import_export.admin import ImportExportModelAdmin

from caltrate import models



class UserAdmin(admin.ModelAdmin):
    list_display = ('openid', 'headimg', 'nickname', 'sex','country', 'province', 'city')

    def headimg(self, obj):
        if obj.headimgurl:
            return "<img src='%s' width=50 height=50 />"% obj.headimgurl
        else:
            return "None"
    headimg.allow_tags = True

class CrazyTabletAdmin(ImportExportModelAdmin):
    list_display = ("name", "headimg", "gift_category", "cell", "address")

    def headimg(self, obj):
        if obj.user.headimgurl:
            url = reverse("admin:caltrate_user_changelist")
            return "<a href='%s?id=%d'><img src='%s' width=50 height=50 /></a>"% (
                url,
                obj.user.id,
                obj.user.headimgurl)
        else:
            return "None"
    headimg.allow_tags = True


for name in dir(models):
    try:
        m = getattr(models, name)
        if m._meta.app_label == 'caltrate':
            if globals().get(m.__name__ + "Admin"):
                admin.site.register(m, globals()[m.__name__ + 'Admin'])
            else:
                try:
                    admin_class = type("%sAdmin"%m.__name__, (admin.ModelAdmin,), {"list_display": [f.name for f in m._meta.fields]})
                    admin.site.register(m, admin_class)
                except Exception, e:
                    pass
    except Exception, e:
        pass

