# -*- coding:utf-8 -*-
# created:     Thu Mar 17 21:18:57 2016
# filename:    wechat.py
# author:      juntao liu
# email:       jinuljt@gmail.com
# descritpion: 微信相关的接口（易维城）

import json
import logging

import tornado.web

from config import CRAZY_TABLET_GIFT_URL, WECHAT_ENTRANCE_DICT

logger = logging.getLogger("wechat")

class WechatSubscribeHandler(tornado.web.RequestHandler):
    def post(self):
        '''
{
data: {"type": 'text',//消息类型(目前只支持text/news)
"text": "Hello World",//消息文本内容
        "news": [ //图文消息内容，最多支持10条
                 { "title": "Happy Day",//图文标题
l                   "description": "Is Really A Happy Day",//描述
                   "url": "URL",//连接地址
                   "picurl": "PIC_URL"//图片url
                  }
                 ]
       },
error:0,
msg: 'xxxx'
}
        '''
        # TODO  区分关注与领奖关注
        # 领奖关注
        self.write(json.dumps({
            "type": "text",
            "text": u"请点击链接进入奖品领取页面 %s"% CRAZY_TABLET_GIFT_URL,
            "error": 0,
            "msg": "success"
        }))


class WechatEntranceHandler(tornado.web.RequestHandler):
    def get(self):
        openid =  self.get_argument("openid") or ""
        app_name = self.get_argument("app_name") or "default"
        redirect_url = WECHAT_ENTRANCE_DICT.get(app_name) or "/"
        if not openid:
            logger.error("openid is none, redirect to /")
            self.redirect("/")
        else:
            self.set_secure_cookie("openid", openid)
            logger.info("openid:%s app_name:%s redirect to %s",
                         openid,
                         app_name,
                         redirect_url)
            self.redirect(redirect_url)

