# -*- coding:utf-8 -*-
# created:     Thu Mar 17 21:13:36 2016
# filename:    crazytablet.py
# author:      juntao liu
# email:       jinuljt@gmail.com
# descritpion:  疯狂的钙片handler

import json
import urllib
import logging
import time
from hashlib import md5, sha1
from  uuid import uuid4
import random

import tornado.web
import tornado.gen
from tornado.httpclient import AsyncHTTPClient

from config import (CRAZY_TABLET_ENTRANCE_URL, SOCIALJIA_WECHAT_API_URL,
                    CRAZY_TABLET_GIFT_URL, CRAZY_TABLET_SUBSCRIBE_URL,
                    API_KEY, API_SECRET, WECHAT_APP_ID, GIFT_TOKEN_EXPIRE,
                    crazytablet_db, crazytablet_db_lock,
                    redis,)
from tools import wechat_redirect, build_query_string
from forms import CrazyTabletGiftForm, CrazyTabletRedeemForm

logger = logging.getLogger("crazytablet")

class CrazyTableBaseHandler(tornado.web.RequestHandler):
    def ensure_wechat_auth(self):
        openid = self.get_secure_cookie("openid")
        if not openid:
            logger.info("openid cookie not exists, redirect to wechat auth")
            self.redirect(wechat_redirect("crazytablet"))
            return False

        # 创建微信用户
        if not crazytablet_db.get("SELECT * FROM user WHERE openid=%s", openid):
            crazytablet_db.execute("INSERT INTO user(openid) VALUES(%s)",
                                   openid)
        logger.info("openid %s exists, pass to crazytablet", openid)
        return True

    @tornado.gen.coroutine
    def is_wechat_subscribe(self):
        openid = self.get_secure_cookie("openid")
        now = int(time.time())
        sig = md5(API_KEY+API_SECRET+str(now)).hexdigest()

        parameters = {"a": "User",
                      "m": "get",
                      "apiKey": API_KEY,
                      "timestamp": now,
                      "openid": openid,
                      "sig": sig}
        http_client = AsyncHTTPClient()
        body = urllib.urlencode(parameters)
        resp = yield http_client.fetch(
            SOCIALJIA_WECHAT_API_URL,
            method="POST",
            body=body)
        logger.info("url:%s body:%s response:%s",
                    SOCIALJIA_WECHAT_API_URL,
                    body,
                    resp.body)

        data = json.loads(resp.body)
        if not data['error'] and data['data']['subscribe']:
            # 记录用户信息
            crazytablet_db.execute(
                u'''UPDATE user
                SET nickname=%s, sex=%s, province=%s, city=%s, country=%s, headimgurl=%s
                WHERE openid=%s
                ''',
                data['data']['nickname'],
                data['data']['sex'],
                data['data']['province'].encode("utf8"),
                data['data']['city'].encode("utf8"),
                data['data']['country'].encode("utf8"),
                data['data']['headimgurl'],
                openid
            )
            raise tornado.gen.Return(True)
        else:
            raise tornado.gen.Return(False)

    def is_redeemed(self, openid):
        if crazytablet_db.get(
                '''SELECT *
                FROM crazytablet, user
                WHERE user.openid = %s and  crazytablet.user_id = user.id''',
                openid):
            return True
        return False

    def get_score(self):
        score = redis.get("SCORE_%s"% self.get_secure_cookie("openid"))
        if not score:
            return (0, 0)
        score = json.loads(score)
        return (score['category'], score['tablet'])

    def update_score(self, category, tablet):
        redis.set("SCORE_%s"% self.get_secure_cookie("openid"),
                  json.dumps({"category": category,
                              "tablet": tablet})
        )

    @tornado.gen.coroutine
    def get_js_config(self):
        now = int(time.time())
        sig = md5(API_KEY+API_SECRET+str(now)).hexdigest()

        parameters = {"a": "Base",
                      "m": "getJsApiTicket",
                      "apiKey": API_KEY,
                      "timestamp": now,
                      "sig": sig}
        http_client = AsyncHTTPClient()
        body = urllib.urlencode(parameters)
        resp = yield http_client.fetch(
            SOCIALJIA_WECHAT_API_URL,
            method="POST",
            body=body)
        logger.info("url:%s body:%s response:%s",
                    SOCIALJIA_WECHAT_API_URL,
                    body,
                    resp.body)
        data = json.loads(resp.body)
        jsconfig = {"timestamp": now,
                    "noncestr": uuid4().hex,
                    "url": self.request.full_url()}
        if not data['error'] and data['data']:
            jsconfig['jsapi_ticket'] = data['data']
            query_string = build_query_string(**jsconfig)
            logger.info("jsapi query string:%s"% query_string)
            jsconfig['signature'] = sha1(query_string).hexdigest()
            jsconfig['appid'] = WECHAT_APP_ID
        logger.info("js config:%s"% str(jsconfig))

        raise tornado.gen.Return(jsconfig)

class CrazyTabletEntranceHandler(CrazyTableBaseHandler):
    @tornado.gen.coroutine
    def get(self):
        if not self.ensure_wechat_auth(): return
        jsconfig = yield self.get_js_config()
        subscribe = yield self.is_wechat_subscribe()
        if not subscribe:
            logger.info("user play game, but not subscribed")

        openid = self.get_secure_cookie("openid")
        if not crazytablet_db.get("SELECT * FROM user WHERE openid=%s", openid):
            crazytablet_db.execute("INSERT INTO user(openid) VALUES(%s)", openid)
        self.set_secure_cookie("token", uuid4().hex, GIFT_TOKEN_EXPIRE)
        self.render(
            "crazytablet/tour.html",
            jsconfig=jsconfig,
        )

class CrazyTabletGiftHandler(CrazyTableBaseHandler):
    @tornado.gen.coroutine
    def get(self):
        if not self.ensure_wechat_auth(): return

        # 更新score
        form = CrazyTabletGiftForm(self.request.arguments)
        if form.validate() and self.get_secure_cookie("token"):
            self.update_score(form.data['category'],
                              form.data['tablet'])
        self.clear_cookie("token")

        # 读取score
        category_id, tablet = self.get_score()


        # 判断是否订阅
        subscribe = yield self.is_wechat_subscribe()
        if not subscribe:
            logger.info("not subscribed redirect to qr page")
            self.redirect(CRAZY_TABLET_SUBSCRIBE_URL)
            return



        logger.info("category:%d tablet:%d", category_id, tablet)

        # 判断礼物ID是否存在
        category = crazytablet_db.get("SELECT * FROM gift_category WHERE id=%s",
                                      category_id)
        if not category:
            logger.error("gift category(%d) id not exists", category_id)
            self.redirect(CRAZY_TABLET_ENTRANCE_URL)
            return

        status = 0
        if self.is_redeemed(self.get_secure_cookie("openid")):
            # 用户是否已经兑换礼物
            status = 4
        elif category.tablet > tablet:
            # 用户钙片是否不够
            status = 3
        elif category.count >= category.max:
            # 礼品是否不够
            status = 2
        logger.info("category:%d status:%d", category.id, status)


        jsconfig = yield self.get_js_config()
        self.render(
            "crazytablet/award_center.html",
            category=category.id,
            status=status,
            jsconfig=jsconfig,
        )

class CrazyTabletSubscribeHandler(CrazyTableBaseHandler):
    def get(self):
        if not self.ensure_wechat_auth(): return
        self.render("crazytablet/subscribe.html")

class CrazyTabletRedeemHandler(CrazyTableBaseHandler):
    @tornado.gen.coroutine
    def post(self):
        if not self.ensure_wechat_auth(): return
        jsconfig = yield self.get_js_config()
        subscribe = yield self.is_wechat_subscribe()
        if not subscribe:
            logger.info("not subscribed redirect to qr page")
            self.redirect(CRAZY_TABLET_SUBSCRIBE_URL)
            return

        form = CrazyTabletRedeemForm(self.request.arguments)
        if not form.validate():
            logger.error("form validate error:%s", str(form.errors))
            self.redirect(CRAZY_TABLET_GIFT_URL)
            return

        openid = self.get_secure_cookie('openid')
        category_id, tablet = self.get_score()

        # 判断category是否存在
        category = crazytablet_db.get("SELECT * FROM gift_category WHERE id=%s",
                                      category_id)
        user = crazytablet_db.get("SELECT * FROM user WHERE openid=%s", openid)
        if not category:
            logger.error("gift category(%d) id not exists", category_id)
            self.redirect(CRAZY_TABLET_ENTRANCE_URL)
            return


        status = 0
        if self.is_redeemed(openid):
            status = 4
        elif category.tablet > tablet:
            status = 3
        else:
            crazytablet_db_lock.execute("set autocommit=0")
            try:
                crazytablet_db_lock.execute("set autocommit=0")
                row = crazytablet_db_lock.get('''SELECT count, max
                FROM gift_category WHERE id=%s FOR UPDATE''',
                                 category.id)
                if row.count >= row.max:
                    status = 2
                else:
                    crazytablet_db_lock.execute('''UPDATE gift_category
                    SET count=count+1
                    WHERE id=%s''',
                                   category.id)
                    crazytablet_db_lock.execute(
                        '''INSERT INTO crazytablet(name,cell,address,
                        user_id,gift_category_id)
                        VALUES(%s, %s, %s, %s, %s)''',
                        form.data['name'],
                        form.data['cell'],
                        form.data['address'],
                        user.id,
                        category.id)
                    status = 1
            except Exception, e:
                logger.error("error due to %s", str(e))
            finally:
                crazytablet_db_lock.execute("commit")
        self.render(
            "crazytablet/award_center.html",
            category=category.id,
            status=status,
            jsconfig=jsconfig,
        )


class CrazyTabletRandomTourHandler(tornado.web.RequestHandler):
    def get(self):
        hotspots = [("12.872", "32.557"),
		            ("28.274", "3.511"),
		            ("-93.093", "44.198"),
		            ("97.936", "20.544"),
		            ("-173.317", "26.302"),
		            ("176.806", "2.909"),
		            ("-81.068", "7.659"),
		            ("263.828", "78.791"),
		            ("-82.624", "-39.052"),
		            ("-173.445", "-27.415"),
		            ("187.316", "-60.832"),
		            ("-104.522", "-52.943"),
		            ("107.299", "-40.326"),
		            ("-19.286", "-26.336"),
		            ("53.418", "-34.289"),
		            ("363.250", "-63.602"),
		            ("-57.900", "-12.034"),
		            ("-112.012", "-4.755"),
		            ("130.109", "14.857"),]
        random.shuffle(hotspots)
        self.render("crazytablet/tour.xml",
                    hotspots=hotspots[:15],
        )
