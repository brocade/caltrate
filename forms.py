# -*- coding:utf-8 -*-
# created:     Tue Mar 22 17:26:24 2016
# filename:    forms.py
# author:      juntao liu
# email:       jinuljt@gmail.com
# descritpion: 


from wtforms import (IntegerField, StringField,
                     validators)
from wtforms.validators import InputRequired, Length
from wtforms_tornado import Form


class CrazyTabletGiftForm(Form):
    category = IntegerField("category",
                            validators=[InputRequired()])
    tablet = IntegerField("tablet",
                          validators=[InputRequired()])


class CrazyTabletRedeemForm(Form):
    name = StringField("name", validators=[InputRequired(), Length(min=1, max=45)])
    cell = StringField("cell", validators=[InputRequired(), Length(min=1, max=45)])
    address = StringField("address")
