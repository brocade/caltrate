# -*- coding:utf-8 -*-
# created:     Fri Mar 18 17:37:23 2016
# filename:    tools.py
# author:      juntao liu
# email:       jinuljt@gmail.com
# descritpion:

import urllib

from config import SOCIALJIA_WECHAT_LOGIN_URL, WECHAT_ENTRANCE_URL

def wechat_redirect(name):
    return (SOCIALJIA_WECHAT_LOGIN_URL +
            urllib.quote(WECHAT_ENTRANCE_URL + "?app_name=" + name))


def build_query_string(**kwargs):
    args = []
    for k,v in sorted(kwargs.iteritems()):
        try:
            qv = v.encode("utf8") if isinstance(v, unicode) else str(v)
        except Exception:
            qv = str(v)
        args.append("%s=%s"% (k, qv))
    return '&'.join(args)
