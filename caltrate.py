# -*- coding:utf-8 -*-
# created:     Thu Mar 17 20:37:33 2016
# filename:    caltrate.py
# author:      juntao liu
# email:       jinuljt@gmail.com
# descritpion: 钙尔奇接易维城二次开发。

import logging

import tornado.ioloop
import tornado.web
from tornado.options import define, parse_command_line, options

from handler.crazytablet import (CrazyTabletEntranceHandler, CrazyTabletGiftHandler,
                                 CrazyTabletSubscribeHandler, CrazyTabletRedeemHandler,
                                 CrazyTabletRandomTourHandler)
from handler.wechat import WechatSubscribeHandler, WechatEntranceHandler

from config import CRAZY_TABLET_ENTRANCE_URL

logger = logging.getLogger("caltrate")


define("addr", default="127.0.0.1", help="listen address")
define("port", default=8000, help="listen port")

parse_command_line()

DEBUG = True

AUTO_RELOAD = True if DEBUG else False


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        logger.warning("should not access /, redirect to crazytablet")
        self.redirect(CRAZY_TABLET_ENTRANCE_URL)


app =  tornado.web.Application(
    [(r"/", MainHandler),
     (r"/crazytablet/entrance", CrazyTabletEntranceHandler),
     (r"/crazytablet/tour.xml", CrazyTabletRandomTourHandler),
     (r"/crazytablet/gift", CrazyTabletGiftHandler),
     (r"/crazytablet/subscribe", CrazyTabletSubscribeHandler),
     (r"/crazytablet/redeem", CrazyTabletRedeemHandler),
     (r"/wechat/entrance", WechatEntranceHandler),
     (r"/wechat/subscribe", WechatSubscribeHandler)],
    cookie_secret="KDo:BmOXz0A5K.P@PQ2lIMO7M;hWzpIN",
    autoreload=AUTO_RELOAD,
    template_path="templates",
)

if __name__ == '__main__':
    app.listen(
        options.port,
        address=options.addr
    )
    logger.info("caltrate project start listen %s:%d", options.addr, options.port)
    tornado.ioloop.IOLoop.current().start()
