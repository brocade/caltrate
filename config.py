# -*- coding:utf-8 -*-
# created:     Thu Mar 17 21:01:36 2016
# filename:    config.py
# author:      juntao liu
# email:       jinuljt@gmail.com
# descritpion: 配置文件

import urlparse

from redis import Redis

import torndb


WECHAT_APP_ID = "wx233ae5112d6ec8f9"
API_KEY = "981e954067322967f81159e59c6b6e91"
API_SECRET = "a893ca1be9dc1faa36b036de07b8b910"

DOMAIN = "http://geq.socialjia.com"

GIFT_TOKEN_EXPIRE = 30*60

CRAZY_TABLET_ENTRANCE_URL = urlparse.urljoin(DOMAIN, "/crazytablet/entrance")
CRAZY_TABLET_GIFT_URL = urlparse.urljoin(DOMAIN, "/crazytablet/gift")
CRAZY_TABLET_SUBSCRIBE_URL = urlparse.urljoin(DOMAIN, "/crazytablet/subscribe")

WECHAT_ENTRANCE_URL = urlparse.urljoin(DOMAIN,  "/wechat/entrance")

SOCIALJIA_WECHAT_LOGIN_URL = "http://call.socialjia.com/Wxapp/weixin_common/oauth2.0/link.php?entid=128&url="
SOCIALJIA_WECHAT_API_URL = "http://api.socialjia.com/index.php"


WECHAT_ENTRANCE_DICT = {
    "crazytablet": CRAZY_TABLET_ENTRANCE_URL,
    "default": "/",
}


DB_HOST = "localhost"
DB_NAME = "caltrate"
DB_USER = "caltrate"
DB_PASSWORD = "caltrate"
DB_CHARSET = "utf8mb4"

crazytablet_db = torndb.Connection(DB_HOST, DB_NAME, DB_USER, DB_PASSWORD,
                                   max_idle_time=7*60,
                                   charset=DB_CHARSET)
crazytablet_db_lock = torndb.Connection(DB_HOST, DB_NAME, DB_USER, DB_PASSWORD,
                                        max_idle_time=7*60,
                                        charset=DB_CHARSET)

redis = Redis(host="127.0.0.1", db="1")
